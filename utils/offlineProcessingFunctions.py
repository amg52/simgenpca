from numpy import isfinite, cos, pi, sqrt, resize, array, cov, matrix, allclose
import sys, detrend2, detrendmmm
import numpy.ma as ma
from pyclimate.svdeofs import svdeofs
from scipy.stats import scoreatpercentile as quant

# x is (t, y, x) array
# returns masked arrays
def detrendWrapper(x, method = 1, yr0 = 1901, sres = 'rcp45'):
    # check if masked array
    if not ma.isMaskedArray(x):
        print 'detrendWrapper: Input must be masked array'
        sys.exit(1)
    # preserve original mask
    y = x.copy()
    c0 = x[0].copy()
    c1 = c0.copy()
    # detrend data location by location
    for i in range(x.shape[1]):
        for j in range(x.shape[2]):
            if not x.mask[0][i, j]:
                if method == 1 or method == 3: # mmm or mixed detrending
                    y[:, i, j], line, coef = detrend2.dt(x[:, i, j])
                elif method == 2:
                    y[:, i, j], line, coef = detrendmmm.dt(x[:, i, j], yr0, sres)
                c0[i, j] = coef[0]
                c1[i, j] = coef[1]
    return (y, c0, c1)

# weighting function
def weight(x, lats):
    sm = x.shape
    wts = sqrt(cos(pi * lats / 180.))      # why sqrt again?
    wts[lats == 90] = 0.
    wts = len(lats) * wts / wts.sum()      # sum = orig sum
    wts = resize(wts, (sm[2], sm[1])).T    # (lat, lon) shape
    wts = resize(wts, sm)                  # same for every time step
    return wts * x

# helps convert EOF in form of vector into map
# mp should be properly masked?
def vec2map(x, mp):
    # check if masked array
    if not ma.isMaskedArray(mp):
        print 'vec2map: Input map must be masked array'
        sys.exit(1)
    if mp.count() != len(x):
        print 'vec2map: Number of unmasked elements in map must agree with length of vector'
        sys.exit(1)
    outmp = mp.copy()
    outmp[~outmp.mask] = x
    return outmp

# computes 2D mean and covariance from 3D distribution
# conditioned on value for one of variables
# mu is original 3D mean
# sig is original 3D covariance matrix
# x is assigned value
# ix is which of three indices x represents (1, 2, or 3)
# see http://j.mp/bBdppe
def condDist(mu, sig, x, ix):
    C = matrix(sig)
    CI = C.I
    anom = matrix(x - mu[ix])
    
    if ix == 0:
        mu1 = matrix([mu[1 :]]).T
        sig11 = C[1 :, 1 :]
        sig12 = C[1 :, 0]
        sig21 = C[0, 1 :]
        sig22 = matrix(C[0, 0])
        ci = CI[1:,1:]
    elif ix == 1:
        mu1 = matrix([[mu[0], mu[2]]]).T
        sig11 = matrix([ [C[0, 0], C[0, 2]], [C[2, 0], C[2, 2]] ])
        sig12 = matrix([ [C[0, 1], C[2, 1]] ]).T
        sig21 = matrix([ [C[1, 0], C[1, 2]] ])
        sig22 = matrix([C[1, 1]])
        ci = matrix(CI[: 2, : 2])
        ci[0,1] = CI[0, 2]
        ci[1,0] = CI[2, 0]
        ci[1,1] = CI[2, 2]
    elif ix == 2:
        mu1 = matrix([mu[: 2]]).T
        sig11 = C[: 2, : 2]
        sig12 = C[: 2, 2]
        sig22 = matrix(C[2, 2])
        sig21 = C[2, : 2]
        ci = matrix(CI[: 2, : 2])

    munew = mu1 + sig12 * sig22.I * anom
    signew0 = ci.I
    signew1 = sig11 - sig12 * sig22.I * sig21

    if allclose(signew0, signew1):
        return munew, signew0
    else:
        print 'condDist: Encountered error'
        sys.exit(1)

# makes multivariate 2D array from two or three 3D masked arrays
# arr0, arr1, arr2 are (t, y, x) arrays
# dt indicates detrending: 0 = None, 1 = Linear, 2 = MMM-based, 3 = Mixed
# stdz = 1 for standardization (if variables are different)
# returns big (t, loc) array of all variables
def makeBig(arr0, arr1, lats, stdz = 1, areawt = 1, arr2 = None, \
        dt = 1, yr0 = 1901, sres = 'rcp45'):
    # indicates whether arr2 is specified
    isArr2 = arr2 is not None
    
    # get shapes
    s0 = arr0.shape
    s1 = arr1.shape
    if isArr2: s2 = arr2.shape

    # error checking
    if s0[0] != s1[0]:
        print 'makeBig: Arrays must have same time dimension!'
        sys.exit(1)
    if isArr2:
        if s0[0] != s2[0]:
            print 'makeBig: Arrays must have same time dimension!'
            sys.exit(1)

    # get number of unmasked values in arrays
    count0 = isfinite(arr0[0]).sum() # isfinite still works for masked arrays
    count1 = isfinite(arr1[0]).sum()

    # number of columns of big array
    ncols = count0 + count1 + (isfinite(arr2[0]).sum() if isArr2 else 0)

    # first detrend (save coefficients for later)
    coef0ar = []
    coef1ar = []
    if dt != 0:
        # for storing b0 and b1 coefficients
        coef0ar = ma.resize(arr0[0].copy(), (3, s0[1], s0[2]))
        coef1ar = coef0ar.copy()
        
        # detrend arrays
        (arr0dt, coef0ar[0, :, :], coef1ar[0, :, :]) = detrendWrapper(arr0, dt, yr0, sres)
        (arr1dt, coef0ar[1, :, :], coef1ar[1, :, :]) = detrendWrapper(arr1, dt, yr0, sres)
        if isArr2: (arr2dt, coef0ar[2, :, :], coef1ar[2, :, :]) = detrendWrapper(arr2, dt, yr0, sres)

        arr0 = arr0dt;
        arr1 = arr1dt
        if isArr2: arr2 = arr2dt

    # standardize (performed prior to area-weighting)
    meanar = []
    stdar = []
    if stdz == 1:
        # calculate means and standard deviations
        meanar = ma.resize(arr0[0].copy(), (3, s0[1], s0[2]))
        stdar = meanar.copy()
        meanar[0] = arr0.mean(axis = 0)
        stdar[0] = arr0.std(axis = 0)
        meanar[1] = arr1.mean(axis = 0)
        stdar[1] = arr1.std(axis = 0)
        if isArr2:
            meanar[2] = arr2.mean(axis = 0)
            stdar[2] = arr2.std(axis = 0)
        
        # standardize
        arr0 = (arr0 - meanar[0]) / stdar[0]
        arr1 = (arr1 - meanar[1]) / stdar[1]
        if isArr2: arr2 = (arr2 - meanar[2]) / stdar[2]

    # area weight
    if areawt == 1:
        arr0 = weight(arr0, lats)
        arr1 = weight(arr1, lats)
        if isArr2: arr2 = weight(arr2, lats)

    # put the three (detrended, standardized, area weighted) arrays into big array
    bigar = ma.zeros((s0[0], ncols))
    bigar[:, : count0] = arr0[:, ~arr0[0].mask]
    bigar[:, count0 : count0 + count1] = arr1[:, ~arr1[0].mask]
    if isArr2: bigar[:, count0 + count1 :] = arr2[:, ~arr2[0].mask]

    return bigar, meanar, stdar, coef0ar, coef1ar
    
# returns EOFs
# inputs similar to makeBig
# pcs is additional argument: 0 = eigenvalues are PC variances and EOFs are orthonormal,
# 1 = PCs have unit variance and EOFs are orthogonal
def calcEOFs(arr0, arr1, neof, lats, pcs = 0, arr2 = None, \
        stdz = 1, dt = 1, yr0 = 1901, sres = 'rcp45'):
    # indicates whether arr2 is specified
    isArr2 = arr2 is not None

    # call makeBig (above)
    (bigar, meanar, stdar, coef0ar, coef1ar) = \
        makeBig(arr0, arr1, lats, arr2 = arr2, dt = dt, yr0 = yr0, sres = sres)

    # remove mean if not done so already
    if stdz == 1:
        bigarmm = bigar
    elif stdz == 0:
        bigarmm = bigar - bigar.mean(axis = 0)
    else:
        print 'calcEOFs: Received unknown value for stdz'
        sys.exit(1)

    # get number of unmasked values in arrays
    count0 = isfinite(arr0[0]).sum()
    count1 = isfinite(arr1[0]).sum()

    # compute PCs, eigenvalues, and EOFs
    pc, lam, eo = svdeofs(bigarmm, pcscaling = pcs)
    vf = lam / lam.sum()
    
    # where to save EOFs
    eo0 = ma.resize(arr0[0].copy(), (neof, arr0.shape[1], arr0.shape[2]))
    eo1 = ma.resize(arr1[0].copy(), (neof, arr1.shape[1], arr1.shape[2]))
    if isArr2: eo2 = ma.resize(arr2[0].copy(), (neof, arr2.shape[1], arr2.shape[2]))

    # convert EOF vectors into corresponding maps
    for i in range(neof):
        eo0[i] = vec2map(eo[: count0, i], arr0[0])
        eo1[i] = vec2map(eo[count0 : count0 + count1, i], arr1[0])
        if isArr2: eo2[i] = vec2map(eo[count0 + count1 :, i], arr2[2])

    return eo0, eo1, eo2, lam, vf, pc[:, : neof], meanar, stdar, coef0ar, coef1ar
    
# select simulations where n-year mean of designated variable lies at given
# percentile when n-year means of other variables are consistent with
# intervariable covariance
# simN is array with n-year means (beginning at the ix)
# var is either 'pr', 'ma', or 'mi'
# pctile is 100-based percentile value (e.g., 20)
# eps0 is fractional range (* std) within which percentile var must lie
# eps1 is range for other variables
# returns lists of candidate simulation indices and variable values at those indices
def qsearch(simN, var, pctile, eps0 = 0.02, eps1 = 0.05):
    # find variable index
    ixdic = {'pr': 0, 'ma': 1, 'mi': 2}
    ixk = ixdic.values()
    ixk.sort()
    varix = ixdic[var]
    ixk.pop(ixk.index(varix))

    # find distribution of other variables conditioned on var
    x = quant(simN[:, varix], pctile)
    munew, signew = condDist(simN.mean(axis = 0), cov(simN.T), x, varix)
    
    # find variable windows
    stds = simN.std(axis = 0)
    epsvar   = eps0 * stds[varix]
    epsoth0  = eps1 * stds[ixk[0]]
    epsoth1  = eps1 * stds[ixk[1]]
    limsvar  = array([x - epsvar, x + epsvar])
    limsoth0 = array([munew[0] - epsoth0, munew[0] + epsoth0])
    limsoth1 = array([munew[1] - epsoth1, munew[1] + epsoth1])

    # search for candidate within window
    candidates = []
    for i in range(simN.shape[0]):
        if limsvar[0] < simN[i, varix] < limsvar[1] and \
           limsoth0[0] < simN[i, ixk[0]] < limsoth0[1] and \
           limsoth1[0] < simN[i, ixk[1]] < limsoth1[1]:
            candidates.append((i, simN[i, :]))

    return candidates