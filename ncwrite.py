"""

Put a bunch of (ntime,nlats,nlons) variables in a single netCDF file
and write the sucker to disk. Not simple!

Usage: w(datadic,dunits,lats,lons,tix,tunits,fname), where

datadic is a dictionary of (T,Y,X) or (Y,X) arrays,
vname is (are) the variable name(s), as a list,
dunits is a dictionary with keys matching those of datadic,
lats, lons are arrays,
tix are time axis values,
tunits the units for time and
fname (excluding the .nc suffix) is the filename to write to.

If writing a (Y,X) array (i.e., with no time dimension), set tix to
None. This is what the script will be looking for.

"""

from netCDF4 import Dataset as nc

def w(datadic,dunits,lats,lons,tix,tunits,fname):
    """ datadic is a bunch of either (T,Y,X) 3-D or (Y,X) 2-D arrays """
    rootgrp = nc(fname+'.nc', 'w', format='NETCDF3_CLASSIC')
    print 'Creating file with format', rootgrp.file_format

    if tix is not None:
        print 'Dataset has a time dimension!'
        nt = len(tix)
        ntchk = datadic[datadic.keys()[0]].shape[0]
        if nt != ntchk:
            raise ValueError('Length of tix does not correspond to time\
            dimension in arrays!')
        if isinstance(tix,unicode):
            tix = str(tix)
        else:
            pass
        rootgrp.createDimension('time', nt)
        times = rootgrp.createVariable('time', 'f8',('time',))
        if type(tunits) == unicode:
            times.units = str(tunits)
        elif type(tunits) == str:
            pass
        times[:] = tix
        
    rootgrp.createDimension('lat', len(lats))
    rootgrp.createDimension('lon', len(lons))

    print 'Dimensions:'
    for i in rootgrp.dimensions:
        print i

    print 'Dimension sizes, unlimitedness:'
    for i, j in rootgrp.dimensions.iteritems():
        print ('%3s %3d %5s' % (i, len(j), j.isunlimited()))

    latitudes  = rootgrp.createVariable('lat', 'f4',('lat',))
    longitudes = rootgrp.createVariable('lon', 'f4',('lon',))

    latitudes.units  = 'degrees north'
    longitudes.units = 'degrees east'

    latitudes[:]  = lats
    longitudes[:] = lons

    varnames = datadic.keys()
    ct = 0
    for name in varnames:
        print 'Creating variable', name
        if tix is not None:
            tmp = rootgrp.createVariable(name,'f4',('time','lat','lon',))
            tmp.units = dunits[name]
        else:
            tmp = rootgrp.createVariable(name,'f4',('lat','lon',))
            tmp.units = dunits[name]

        tmp[:] = datadic[name]
        ct += 1

    rootgrp.close()

    print 'File is written!'

######################
             
