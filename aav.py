"""
Area-average a 3-D dataset, returning 1-D
"""

import numpy as np
import sys

def av(dat,lats):
    """ Need to account for masked values, which may change the number
    and sum of weights that are applied at a given longitude..."""
    
    sd = dat.shape
    if len(sd) != 3:
        print 'Expecting 3-D data! Aborting!'
        sys.exit(1) 
           
    wts = np.cos(2.*np.pi*lats/360.)         # 1-D, one value per latitude
    wtsre0 = np.resize(wts,(sd[2],sd[1])).T
    wtsre  = np.resize(wtsre0,sd)            # Same shape as dat
    tmp0 = dat*wtsre                         # Still masked

    if not 'mask' in dir(dat):
        tmp1 = tmp0.mean(axis=2).mean(axis=1)
    else:
        tmp1 = tmp0.sum(axis=1).sum(axis=1) / ((1 - dat.mask) * wtsre).sum(axis=1).sum(axis=1)
        
    return tmp1