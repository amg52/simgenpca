"""

Area-average from the processed ascii files. latlist is a list of strings, from the first line
of one of the ascii files in sims_unzip/ascii. arr is the result of batch processing by 
condition.py (i.e., in a loop over columns) and may be one of .1, .5, or .9, depending on
which file was read. (See condition.py for details.)

The output is an area-weighted time series, computed using the lats in latlist and the
ntimes x npoints arr. (npoints must be of length len(latlist) - 1, since the first entry 
in latlist is the word "latitudes."

"""

from numpy import allclose, cos, ones, pi, resize 

def comp(latlist, arr):
    ll = len(latlist) - 1
    cosar = 1e20*ones(ll)
    for i in range(ll):
        cosar[i] = cos(2. * pi * float(latlist[i+1]) / 360.)

    cosbig = resize(cosar, arr.shape)
    test = allclose(cosbig[0,:], cosbig[10,:])
    if test:
        print 'cosbig seems OK!'
    else:
        raise Exception('Problem with cosbig!')

    prod = arr * cosbig
    prodsum = prod.sum(axis=1) / cosar.sum()
    
    return prodsum

########
    
