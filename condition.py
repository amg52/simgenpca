"""

Prepare seas avs from ascii files

"""

from numpy import zeros

def c(ix, col):
    froot = 'sims_unzip/ascii/sim_pre_SONDJF_0.'
    fname10 = froot + '10_' + ix + '.dat'
    fname50 = froot + '50_' + ix + '.dat'
    fname90 = froot + '90_' + ix + '.dat'

    f10 = open(fname10)
    f50 = open(fname50)
    f90 = open(fname90)

    pr10 = zeros(1200)
    pr50 = zeros(1200)
    pr90 = zeros(1200)
    pr10seas = zeros(100)
    pr50seas = zeros(100)
    pr90seas = zeros(100)


    q = f10.readline()
    q = f50.readline()
    q = f90.readline()
    lats = q.split()
    q = f10.readline()
    q = f50.readline()
    q = f90.readline()
    lons = q.split()
        
    for i in range(1200):
        q = f10.readline()
        ql = q.split()
        pr10[i] = float(ql[col])
        q = f50.readline()
        ql = q.split()
        pr50[i] = float(ql[col])
        q = f90.readline()
        ql = q.split()
        pr90[i] = float(ql[col])

    for i in range(100):
        start = 12*i + 8
        end   = start + 6
        pr10seas[i] = pr10[start:end].mean()
        pr50seas[i] = pr50[start:end].mean()
        pr90seas[i] = pr90[start:end].mean()

    f10.close()
    f50.close()
    f90.close()

    return lats[col], lons[col], pr10seas, pr50seas, pr90seas


#################
