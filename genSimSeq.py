#!/usr/bin/env python

# ***
# assumes script is run from simgen* root directory
# ***

# helper functions
def toMasked(array):
    return npMasked.masked_where(isnan(array), array)
def createMap(axis, lats, lons):
    m = Basemap(projection = 'merc', llcrnrlon = lons[0], llcrnrlat = lats[0], \
            urcrnrlon = lons[-1], urcrnrlat = lats[-1], resolution = 'c', ax = axis)
    m.drawcoastlines()
    m.drawstates()
    m.drawcountries()
    m.drawmapboundary()
    m.drawmeridians(linspace(lons[0], lons[-1], 5))
    m.drawparallels(linspace(lats[0], lats[-1], 5))
    return m

# import and call startup script
import startup
startup

# ======
# PRELIM
# ======

# input options
prFile = 'cdf_nc/CRU/precip_TS2p1_JJAS_WA.cdf' # data files
tmaxFile = 'cdf_nc/CRU/tmax_TS2p1_JJAS_WA.cdf'
tminFile = 'cdf_nc/CRU/tmin_TS2p1_JJAS_WA.cdf'
yearStart = 1901 # first year in data
nEOFs = 10 # number of EOFs to generate
nPCs = 5 # number of PCs to save
pcsFile = 'pcs.txt'
simFile = 'sim.txt'
nSamples = 10000 # number of samples to generate
nYears = 5 # number of years over which to calculate mean
prPercentile = 95
prEpsilon = 0.02
tempEpsilon = 0.05
displayFigs = True
saveFigs = True

# preliminary error checking
import sys
if nPCs > nEOFs:
    print 'genSimSeq: Number of PCs to save must be no greater than number of total EOFs'
    sys.exit(1)
if nSamples < nYears:
    print 'genSimSeq: Number of years over which to average cannot be greater than number of years'
    sys.exit(1)

# import modules
from netCDF4 import Dataset as nc
from numpy import isnan, savetxt, loadtxt, zeros, arange, linspace, array
import numpy.ma as npMasked
import offlineProcessingFunctions as opf
import rpy2.robjects as robjects, recon
from aav import av as areaAverage # taken from simgenpca but shouldn't do this
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from pylab import savefig

# close all existing figures
plt.close('all')

# load data
prf = nc(prFile)
tmaxf = nc(tmaxFile)
tminf = nc(tminFile)

# extract variables
pr = toMasked(prf.variables['prcp'][:])
tmax = toMasked(tmaxf.variables['temp'][:])
tmin = toMasked(tminf.variables['temp'][:])
lats = prf.variables['Y'][:]
lons = prf.variables['X'][:]
totalYears = pr.shape[0]
obsYears = arange(yearStart, yearStart + totalYears)

# variable names
var = ['$P$ (mm/mon)', '$T_{max}$ ($^\circ$C)', '$T_{min}$ ($^\circ$C)']

# ======
# STEP 1
# ======

# assumes pcs = 0, stdz = 1, dtin = 1
(eofPr, eofTmax, eofTmin, eigen, vf, pcs, meanArray, stdArray, coeff0, coeff1) = \
    opf.calcEOFs(pr, tmax, nEOFs, lats, arr2 = tmin, yr0 = yearStart)

# save PCs to file
if nPCs > pcs.shape[1]:
    print 'genSimSeq: Selected too many PCs'
    sys.exit(1)    
savetxt(pcsFile, pcs[:, :nPCs])

if displayFigs:
    # plot PCs
    plt.figure()
    plt.plot(obsYears, pcs)
    plt.xlabel('Year')
    plt.ylabel('Principal Components')
    plt.xlim(yearStart, yearStart + totalYears)
    plt.xticks(range(yearStart, yearStart + totalYears, 20))
    plt.show()
    if saveFigs: savefig('fig1.png')

    # plot first five EOFs
    nEOFsToPlot = 5 if nEOFs >= 5 else nEOFs
    fig, axes = plt.subplots(nrows = nEOFsToPlot, ncols = 3, figsize = (10, 10))
    for i in range(axes.shape[0]):
        for j in range(axes.shape[1]):
            m = createMap(axes[i, j], lats, lons)
            if i == 0 and j == 0:
                # only perform once
                glons, glats = m.makegrid(len(lons), len(lats))
                x, y = m(glons, glats)
            plotData = eofPr if j == 0 else (eofTmax if j == 1 else eofTmin)
            cLower = npMasked.min(plotData)
            cUpper = npMasked.max(plotData)
            cs = m.contourf(x, y, plotData[i], linspace(cLower, cUpper, 100))
            cbar = m.colorbar(cs, location = 'right', format = '%.3f', ticks = linspace(cLower, cUpper, 3)) 
            axes[i, j].set_title(var[j] + ' EOF ' + str(i + 1))
    fig.tight_layout()
    fig.show()
    if saveFigs: savefig('fig2.png')

    # plot mean of variables
    # (should be close to zero because detrended)
    fig, axes = plt.subplots(nrows = 3, ncols = 1, figsize = (10, 10))
    for i in range(len(axes)):
        m = createMap(axes[i], lats, lons)
        if i == 0:
            # perform only once
            glons, glats = m.makegrid(len(lons), len(lats))
            x, y = m(glons, glats)
        cs = m.contourf(x, y, meanArray[i])
        cbar = m.colorbar(cs, location = 'right', format = '%.3e') 
        axes[i].set_title(var[i] + ' $\mu$')
    fig.tight_layout()
    fig.show()
    if saveFigs: savefig('fig3.png')

    # plot standard deviation of variables
    fig, axes = plt.subplots(nrows = 3, ncols = 1, figsize = (10, 10))
    for i in range(len(axes)):
        m = createMap(axes[i], lats, lons)
        if i == 0:
            # perform only once
            glons, glats = m.makegrid(len(lons), len(lats))
            x, y = m(glons, glats)
        cs = m.contourf(x, y, stdArray[i])
        cbar = m.colorbar(cs, location = 'right', format = '%.3f') 
        axes[i].set_title(var[i] + ' $\sigma$')
    fig.tight_layout()
    fig.show()
    if saveFigs: savefig('fig4.png')

# ======
# STEP 2
# ======

# define R functions
robjects.r('''
    loadData <- function(pcsFile) {
        # read data from file
        data <- read.table(pcsFile)
        return(data)
    }
    createModel <- function(data) {
        # estimate VAR TSmodel from data
        library(dse)
        tsdat <- TSdata(output = data)
        mdl <- estVARXls(tsdat, max.lag = 1)
        return(mdl)
    }
    simulatePCs <- function(mdl, simFile, nSamples) {
        # simulate from model
        library(dse)
        sim <- simulate(mdl, sampleT = nSamples)
        write(t(sim$output), file = simFile, ncolumns = dim(mdl$data$output)[2])
    }
    ''')

# load R functions
r_loadData = robjects.globalenv['loadData']
r_createModel = robjects.globalenv['createModel']
r_simulatePCs = robjects.globalenv['simulatePCs']

# run R functions to generate simulated data
r_pcs = r_loadData(pcsFile)
r_mdl = r_createModel(r_pcs)
r_simulatePCs(r_mdl, simFile, nSamples)

if displayFigs:
    # plot data and estimates
    estimates = array(robjects.r('%s$estimates' %(r_mdl.r_repr()))[2])
    fig, axes = plt.subplots(nrows = nPCs, ncols = 1, figsize = (10, 10))
    for i in range(len(axes)):
        axes[i].plot(obsYears, pcs[:, i], 'b.-', label = 'Obs')
        axes[i].plot(obsYears, estimates[:, i], 'rx-', label = 'Est')
        axes[i].set_xlabel('Year')
        axes[i].set_ylabel('PC ' + str(i + 1))
        axes[i].set_xlim(yearStart, yearStart + totalYears)
        axes[i].set_xticks(range(yearStart, yearStart + totalYears, 20))
        yticks = linspace(axes[i].get_ylim()[0], axes[i].get_ylim()[1], 4)
        axes[i].set_yticks(yticks)
        axes[i].set_yticklabels(['%.2f' % e for e in yticks])
        axes[i].legend()
    fig.tight_layout()
    fig.show()
    if saveFigs: savefig('fig5.png')

# ======
# STEP 3
# ======

# reconstruct data and area average
simulatedPCs = loadtxt(simFile)
reconData = npMasked.empty([nSamples, 3])
reconPr = recon.r1(eofPr, simulatedPCs, 'pr', nPCs, meanArray, stdArray, lats)
reconData[:, 0] = areaAverage(reconPr, lats)
reconTmax = recon.r1(eofTmax, simulatedPCs, 'tmx', nPCs, meanArray, stdArray, lats)
reconData[:, 1] = areaAverage(reconTmax, lats)
reconTmin = recon.r1(eofTmin, simulatedPCs, 'tmn', nPCs, meanArray, stdArray, lats)
reconData[:, 2] = areaAverage(reconTmin, lats)

# perform sliding window average
numAverages = nSamples - nYears + 1
simN = zeros((numAverages, 3))
for i in range(numAverages):
    simN[i, :] = reconData[i : i + nYears, :].mean(axis = 0)

if displayFigs:   
    # plot reconstructed data and sliding window average
    fig, axes = plt.subplots(nrows = 3, ncols = 1, figsize = (10, 10))
    for i in range(len(axes)):
        axes[i].plot(range(nSamples), reconData[:, i], 'b', label = 'Recon')
        axes[i].plot(range(numAverages), simN[:, i], 'r', label = 'Avg')
        axes[i].set_ylabel(var[i], multialignment = 'center')
        axes[i].set_xlim(0, nSamples)
        xticks = [round(e) for e in linspace(0, nSamples, 10)]
        axes[i].set_xticks(xticks)
        axes[i].set_xticklabels(['%d' % e for e in xticks])
        axes[i].legend()
    axes[-1].set_xlabel('Simulated Year')
    fig.tight_layout()
    fig.show()
    if saveFigs: savefig('fig6.png')

# find candidate years
candidates = opf.qsearch(simN, 'pr', prPercentile, prEpsilon, tempEpsilon)